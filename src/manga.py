import os
import re

class MangaBase():
    
    def __init__(self, chapters_all=[], chapters_overwrite=False, name=None, url=None):
        self.chapters_all = chapters_all
        self.chapters_overwrite = chapters_overwrite
        self.name = name
        self.url = url


class Chapter():

    def __init__(self, id=None, name=None, num=None, volume=None, page_urls=[]):
        self.id = id
        self.name = name
        self.num = num
        self.volume = volume
        self.page_urls = page_urls
