import logging
import os
import re
import requests

from manga import MangaBase, Chapter
from utils import create_dir, get_url_soup, download_url, raise_err

LOGGER = logging.getLogger(__name__)

class MangaDex(MangaBase):

    def __init__(self, url, chapters_overwrite=False):
        super().__init__(url=url, chapters_overwrite=chapters_overwrite)

    @staticmethod
    def download_chapters(dl_dir, dl_from, dl_to, overwrite_existing, session, stop_on_download_error, url):
        cur_chap_id = dl_from
        
        manga = get_url_soup(url)
        LOGGER.info(manga.prettify())
        if not manga:
            raise_err(msg='Invalid manga link', error_type=EnvironmentError)
        mdex = MangaDex(url=url)

        chapters_pagenum = 1
        #while (1):
        while chapters_pagenum < 3 :
        #while dl_from <= cur_chap_id <= dl_to:
            chapters_list_url = url + '/chapters/' + str(chapters_pagenum)
            chapters_list = get_url_soup(chapters_list_url)
            if not chapters_list:
                print('No more chapters available')
                return

            #re_chap = re.compile('chapter_\d+')
            re_chap = re.compile('/chapter/\d{6,}')

            #chap_list = chapters_list.find_all('tr', attrs={'id':re_chap})
            #chap_list = chapters_list.find_all('div', {'class': "chapter-row d-flex row no-gutters p-2 align-items-center border-bottom odd-row"})
            chap_list = chapters_list.find_all('div', {'data-lang':'1'})

            LOGGER.info(chap_list)
            LOGGER.info('----- START PROCESSING -----')
            for chap_text in chap_list:
                language = chap_text.find('a', {'href':re_chap}) #1: English
                if (language):
                    LOGGER.info(language)
                    c1 = MangaDexChapter()
                    
                    
                    c1.set_metadata(str(chap_text))
                    print(c1.num, c1.id, c1.name)
                    #(cid, volume, num, name) = chapter_metadata(str(chap_text))
                    cur_chap_num = int(c1.num)
                    
                    print(f'{dl_from} <= {cur_chap_num} <= {dl_to}')
                    if not (dl_from <= cur_chap_num <= dl_to or c1.num or c1.id):
                        continue
                    #if not c1.id:
                    #    chapters_pagenum += 1
                    #    break
                    
                    mdex.chapters_all.append(c1.id)
                    cidurl = f'https://mangadex.org/chapter/{c1.id}'
                    vol_path = os.path.join(dl_dir, 'Vol. {0:0>3} Ch. {1:0>4} - {2}'.format(c1.volume, c1.num, c1.name))
                    print(vol_path)
                    
                    chap_local_html = os.path.join(dl_dir, 'html', f'{c1.num}.html')
                    download_url(cidurl, chap_local_html, overwrite_existing=overwrite_existing, session=session, stop_on_error=stop_on_download_error)
                    
                    create_dir(vol_path)
                    #chapter_download(cidurl, num, name, vol_path)
                    print(f'{vol_path}, {cidurl}')

                    c1.page_urls = MangaDex.get_chapimg_urls(html=chap_local_html)
                    print(c1.page_urls)
                    page_id = 0
                    for page_url in c1.page_urls:
                        page_ext = page_url.split('.')[-1]
                        page_local = os.path.join(vol_path, f'ch{c1.num}_{page_id:03}.{page_ext}')
                        print(page_local)
                        download_url(page_url, page_local, session=session, stop_on_error=stop_on_download_error)
                        page_id += 1
                #return
            chapters_pagenum += 1

    @staticmethod
    def get_chapimg_urls(html):
        ''' Get image URLs from each chapter .html
        '''
        dataurl = ''
        images = []
        chapimg_urls = []
        dataurl_pattern = re.compile(r"var dataurl = '([a-zA-Z0-9]+)';")
        page_array_pattern = re.compile(r"\'(.+\.(png|jpg|jpeg))\'")
        server_pattern = re.compile(r"var server = '(http.+)';")
        with open(html, 'r') as f:
            lines = f.readlines()
            for i, line in enumerate(lines):
                if lines[i-1].startswith('var page_array'):
                    imagelines = line.split(',')
                    for il in imagelines:
                        imgmatch = re.match(page_array_pattern, il)
                        if imgmatch:
                            images.append(imgmatch.group(1))
                elif line.startswith('var dataurl'):
                    dataurl = re.findall(dataurl_pattern, line)[0]
                elif line.startswith('var server'):
                    server = re.findall(server_pattern, line)[0]

        #print(server, dataurl, images)
        for image in images:
            chapimg_urls.append(f'{server}{dataurl}/{image}')
        if not chapimg_urls:
            LOGGER.warning('No chapter images found')    
        return chapimg_urls
    
    @staticmethod
    def login(login_data=None):
        ''' Login Mangadex
        '''
        url = 'https://mangadex.org/ajax/actions.ajax.php?function=login'
        header = {'x-requested-with': 'XMLHttpRequest'}
        session = requests.Session()
        LOGGER.info('Logging in')
        r = session.post(url, headers=header, data=login_data)
        LOGGER.info(r.text)
        return session


class MangaDexChapter(Chapter):

    def __init__(self):
        super().__init__()

    def set_metadata(self, chap_html):
        ''' Metadata: 
        title="(.+)" 
        data-chapter-id="(\d+)" 
        data-chapter-num="(\d+)" 
        data-volume-num="(\d+)" 
        data-chapter-name="(.+)" 
        href="(.+)"
        '''
        r_cid = re.search(r'data-id="(\d+?)"', chap_html)
        if r_cid:
            cid = r_cid.group(1)
            self.id = cid

        r_name = re.search(r'data-title=\"(.+?)\"', chap_html)
        if r_name:
            name = r_name.group(1)
            name = re.sub(r'[\\/*?:!"<>|]', '', name)
            name = name.rstrip('.,')
            self.name = name

        r_num = re.search(r'data-chapter="(\d+?)"', chap_html)
        if r_num: 
            self.num = r_num.group(1)

        r_volume = re.search(r'data-volume="(\d*?)"', chap_html)
        if r_volume:
            self.volume = r_volume.group(1)
        
        #folder = 'Vol. {} Ch. {} - {}'.format(volume, num, name)
        #print(folder)
        #return (cid, volume, num, name)