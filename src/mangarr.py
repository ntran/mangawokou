from datetime import datetime
import json
import logging
import os
from pathlib import Path
import re
import requests
import sys
import time

sys.path.append(Path(__file__).parent.absolute())
from manga import Chapter
from mangadex import MangaDex
from utils import create_dir, get_url_soup, download_url, raise_err

LOGGER = logging.getLogger(__name__)
CONFIG_PATH = os.path.join(Path(__file__).resolve().parents[1], '.mangarr', 'config.json')

def mangarr():
    with open(CONFIG_PATH) as f:
        config = json.loads(f.read())
    #print(config)
    login_data = config['mangadex']['account']
    session = None
    #session = MangaDex.login(login_data=login_data)
    mangas = config['mangadex']['mangas']
    stop_on_download_error = config['general']['stop_on_download_error']
    overwrite_existing = config['general']['overwrite_existing']
    for manga in mangas:
        dl_dir = manga['download_dir']
        dl_from = manga['download_from']
        dl_to = manga['download_to']
        url = manga['url']
        print(manga)
        MangaDex.download_chapters(dl_dir, dl_from, dl_to, overwrite_existing, session, stop_on_download_error, url)



if __name__ == '__main__':
    #LOGGER = logging.getLogger('mangarr')
    logfName = 'log_' + datetime.fromtimestamp(time.time()).strftime('%Y%m%d_%H%M%S') + '.log'
    logfile = logging.FileHandler(os.path.join(Path(__file__).resolve().parents[1], '.mangarr', logfName), encoding='utf-8')
    logfile.setLevel(logging.DEBUG)
    LOGGER.addHandler(logfile)
    mangarr()