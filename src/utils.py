import os
import time

from bs4 import BeautifulSoup
import logging
import requests

LOGGER = logging.getLogger(__name__)


def create_dir(d):
    ''' Create folder if it is not existed yet
    '''
    if not os.path.exists(d):
        LOGGER.info('Creating directory:\t{}'.format(d))
        os.makedirs(d)

def download_url(url, dl_path, overwrite_existing=False, session=None, sleep_time=5, stop_on_error=True):
    ''' Download any URL to local
    '''
    if os.path.exists(dl_path) and os.path.isfile(dl_path) and not overwrite_existing:
        LOGGER.info('%s already exists. Skipped.')
        return

    time.sleep(sleep_time)
    LOGGER.info('Download %s to %s', url, dl_path)
    if session:
        r = session.get(url)
    else:
        r = requests.get(url)
    if r.status_code != 200:
        raise_err(msg=f'Bad return code {r.status_code}', error_type=ConnectionError, stop_on_error=stop_on_error)
    with open(dl_path, 'wb') as f:
        for chunk in r.iter_content(1024):
            f.write(chunk)

def get_url_soup(url, sleep_time=5):
    ''' Get BeautifulSoup of HTML file
    '''
    time.sleep(sleep_time)
    LOGGER.info('Get %s', url)
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    if 'are no chapters' in soup.text:
        return None
    else:
        return soup

def raise_err(msg, error_type=RuntimeError, stop_on_error=True):
    LOGGER.error(msg)
    if stop_on_error:
        raise error_type(msg)

def download2(self, chapterurl, num, name, folder, fol_vol=True, fol_chap=True):
    re_img = re.compile('/data/.+?/.+?\.(png|jpg|jpeg)')
    img_num = 1
    fail_img = 0
    if fol_vol is True:
        img_path = os.path.join(folder, 'Volume {0:0>3}'.format(num))

    if fol_chap is True:
        create_dir(os.path.join(folder, 'Chapter {0:0>4} - {1}'.format(num, name)))

    while (1):
        chapter_img = get_url_soup(chapterurl + '/{}'.format(img_num))
        img = chapter_img.find('img', {'src':re_img})
        if (img):
            r_img_loc = re.search(r'src="(.+?)"', str(img))
            print(r_img_loc)
            img_loc = r_img_loc.group(1)
            img_url = 'https://mangadex.org/' + img_loc
            ext = img_loc.split('.')[-1]
            img_path = os.path.join(folder, 'ch{0:0>4}_{1:0>3}.{2}'.format(num, img_num, ext))
            if os.path.exists(img_path):
                print('Skip {}'.format(img_path))
                continue
            print('Downloading {}'.format(img_path))
            #download(img_url, img_path)
        else:
            fail_img += 1
        if fail_img >= 10:
            print("Download chapter complete")
            return
        img_num += 1
    return